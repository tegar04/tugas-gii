<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kegiatans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kegiatan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kegiatan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tanggal_mulai',
            'tanggal_selesai',
            'tempat',
            'deskripsi:ntext',
            //'kepesertaan',
            //'nilai',
            //'jenis_kegiatan_id',
            //'dosen_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
