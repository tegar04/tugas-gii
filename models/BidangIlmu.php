<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bidang_ilmu".
 *
 * @property int $id
 * @property string $nama
 * @property string|null $deskripsi
 *
 * @property PenelitianDosen[] $penelitianDosens
 * @property PenelitianDosen[] $penelitianDosens0
 */
class BidangIlmu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bidang_ilmu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['deskripsi'], 'string'],
            [['nama'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * Gets query for [[PenelitianDosens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenelitianDosens()
    {
        return $this->hasMany(PenelitianDosen::className(), ['dosen_id' => 'id']);
    }

    /**
     * Gets query for [[PenelitianDosens0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenelitianDosens0()
    {
        return $this->hasMany(PenelitianDosen::className(), ['bidang_ilmu_id' => 'id']);
    }
}
